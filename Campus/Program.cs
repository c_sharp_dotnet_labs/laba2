using System;
using System.Windows.Forms;
#if (DEBUG)
using System.Runtime.InteropServices;
#endif

namespace Campus
{
    static class Program
    {
        /// <summary>
        ///  The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
#if (DEBUG)
            if (!AllocConsole())
                MessageBox.Show("Failed");
#endif

            Application.SetHighDpiMode(HighDpiMode.SystemAware);
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainForm());
        }
#if (DEBUG)
        [DllImport("Kernel32.dll")]
        static extern Boolean AllocConsole();
#endif
    }
}
