﻿namespace Campus
{
    partial class MainForm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.roomsNumber = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.lbRoomsNumber = new System.Windows.Forms.Label();
            this.btAddStudent = new System.Windows.Forms.Button();
            this.btRemoveStudent = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.lbStaffNumber = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lbUniversity = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.table = new System.Windows.Forms.DataGridView();
            this.lbStudentNumber = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lbAccommodationPayment = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lbAdress = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btCalculate = new System.Windows.Forms.Button();
            this.dtTo = new System.Windows.Forms.DateTimePicker();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.dtSince = new System.Windows.Forms.DateTimePicker();
            this.checkBox = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.roomsNumber)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.table)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // roomsNumber
            // 
            this.roomsNumber.Location = new System.Drawing.Point(188, 78);
            this.roomsNumber.Maximum = new decimal(new int[] {
            99999,
            0,
            0,
            0});
            this.roomsNumber.Name = "roomsNumber";
            this.roomsNumber.Size = new System.Drawing.Size(60, 29);
            this.roomsNumber.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 80);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(170, 21);
            this.label2.TabIndex = 3;
            this.label2.Text = "Количество комнат:";
            // 
            // lbRoomsNumber
            // 
            this.lbRoomsNumber.AutoSize = true;
            this.lbRoomsNumber.Font = new System.Drawing.Font("Times New Roman", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lbRoomsNumber.Location = new System.Drawing.Point(139, 21);
            this.lbRoomsNumber.Name = "lbRoomsNumber";
            this.lbRoomsNumber.Size = new System.Drawing.Size(263, 31);
            this.lbRoomsNumber.TabIndex = 4;
            this.lbRoomsNumber.Text = "Студенческий кампус";
            // 
            // btAddStudent
            // 
            this.btAddStudent.Location = new System.Drawing.Point(435, 142);
            this.btAddStudent.Name = "btAddStudent";
            this.btAddStudent.Size = new System.Drawing.Size(130, 47);
            this.btAddStudent.TabIndex = 6;
            this.btAddStudent.Text = "Добавить";
            this.btAddStudent.UseVisualStyleBackColor = true;
            // 
            // btRemoveStudent
            // 
            this.btRemoveStudent.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btRemoveStudent.Location = new System.Drawing.Point(435, 86);
            this.btRemoveStudent.Name = "btRemoveStudent";
            this.btRemoveStudent.Size = new System.Drawing.Size(130, 47);
            this.btRemoveStudent.TabIndex = 6;
            this.btRemoveStudent.Text = "Удалить";
            this.btRemoveStudent.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 125);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(196, 21);
            this.label1.TabIndex = 3;
            this.label1.Text = "Количество персонала:";
            // 
            // lbStaffNumber
            // 
            this.lbStaffNumber.AutoSize = true;
            this.lbStaffNumber.Location = new System.Drawing.Point(205, 125);
            this.lbStaffNumber.Name = "lbStaffNumber";
            this.lbStaffNumber.Size = new System.Drawing.Size(19, 21);
            this.lbStaffNumber.TabIndex = 7;
            this.lbStaffNumber.Text = "0";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 167);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(374, 21);
            this.label3.TabIndex = 8;
            this.label3.Text = "Университет, которому кампус принадлежит:";
            // 
            // lbUniversity
            // 
            this.lbUniversity.AutoSize = true;
            this.lbUniversity.Location = new System.Drawing.Point(12, 192);
            this.lbUniversity.Name = "lbUniversity";
            this.lbUniversity.Size = new System.Drawing.Size(53, 21);
            this.lbUniversity.TabIndex = 9;
            this.lbUniversity.Text = "label4";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.table);
            this.groupBox1.Controls.Add(this.lbStudentNumber);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.btAddStudent);
            this.groupBox1.Controls.Add(this.btRemoveStudent);
            this.groupBox1.Location = new System.Drawing.Point(12, 228);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(571, 199);
            this.groupBox1.TabIndex = 10;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Список студентов";
            // 
            // dataGridView
            // 
            this.table.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.table.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.table.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.table.Location = new System.Drawing.Point(8, 53);
            this.table.Name = "dataGridView";
            this.table.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.table.Size = new System.Drawing.Size(421, 136);
            this.table.TabIndex = 9;
            this.table.Text = "dataGridView1";
            // 
            // lbStudentNumber
            // 
            this.lbStudentNumber.AutoSize = true;
            this.lbStudentNumber.Location = new System.Drawing.Point(207, 29);
            this.lbStudentNumber.Name = "lbStudentNumber";
            this.lbStudentNumber.Size = new System.Drawing.Size(53, 21);
            this.lbStudentNumber.TabIndex = 8;
            this.lbStudentNumber.Text = "label5";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(7, 29);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(194, 21);
            this.label4.TabIndex = 7;
            this.label4.Text = "Количество студентов:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(19, 447);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(265, 21);
            this.label5.TabIndex = 11;
            this.label5.Text = "Месячная плата за проживание:";
            // 
            // lbAccommodationPayment
            // 
            this.lbAccommodationPayment.AutoSize = true;
            this.lbAccommodationPayment.Location = new System.Drawing.Point(280, 447);
            this.lbAccommodationPayment.Name = "lbAccommodationPayment";
            this.lbAccommodationPayment.Size = new System.Drawing.Size(53, 21);
            this.lbAccommodationPayment.TabIndex = 12;
            this.lbAccommodationPayment.Text = "label6";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(19, 484);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(255, 21);
            this.label6.TabIndex = 13;
            this.label6.Text = "Адрес студенческого кампуса:";
            // 
            // lbAdress
            // 
            this.lbAdress.AutoSize = true;
            this.lbAdress.Location = new System.Drawing.Point(19, 507);
            this.lbAdress.Name = "lbAdress";
            this.lbAdress.Size = new System.Drawing.Size(53, 21);
            this.lbAdress.TabIndex = 14;
            this.lbAdress.Text = "label7";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btCalculate);
            this.groupBox2.Controls.Add(this.dtTo);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.dtSince);
            this.groupBox2.Location = new System.Drawing.Point(18, 581);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(559, 103);
            this.groupBox2.TabIndex = 15;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Определение прибыли за промежуток:";
            // 
            // btCalculate
            // 
            this.btCalculate.Location = new System.Drawing.Point(202, 63);
            this.btCalculate.Name = "btCalculate";
            this.btCalculate.Size = new System.Drawing.Size(161, 34);
            this.btCalculate.TabIndex = 5;
            this.btCalculate.Text = "Определить";
            this.btCalculate.UseVisualStyleBackColor = true;
            // 
            // dtTo
            // 
            this.dtTo.Location = new System.Drawing.Point(336, 28);
            this.dtTo.Name = "dtTo";
            this.dtTo.Size = new System.Drawing.Size(200, 29);
            this.dtTo.TabIndex = 4;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(301, 33);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(37, 21);
            this.label9.TabIndex = 3;
            this.label9.Text = "По:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(14, 33);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(27, 21);
            this.label8.TabIndex = 2;
            this.label8.Text = "С:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(1423, -96);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(53, 21);
            this.label7.TabIndex = 1;
            this.label7.Text = "label7";
            // 
            // dtSince
            // 
            this.dtSince.Location = new System.Drawing.Point(41, 28);
            this.dtSince.Name = "dtSince";
            this.dtSince.Size = new System.Drawing.Size(200, 29);
            this.dtSince.TabIndex = 0;
            // 
            // checkBox
            // 
            this.checkBox.AutoSize = true;
            this.checkBox.Location = new System.Drawing.Point(26, 539);
            this.checkBox.Name = "checkBox";
            this.checkBox.Size = new System.Drawing.Size(143, 25);
            this.checkBox.TabIndex = 17;
            this.checkBox.Text = "Есть столовая";
            this.checkBox.UseVisualStyleBackColor = true;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 21F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(595, 707);
            this.Controls.Add(this.checkBox);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.lbAdress);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.lbAccommodationPayment);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.lbUniversity);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lbStaffNumber);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lbRoomsNumber);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.roomsNumber);
            this.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.Text = "Студенческий кампус";
            ((System.ComponentModel.ISupportInitialize)(this.roomsNumber)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.table)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.NumericUpDown roomsNumber;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lbRoomsNumber;
        private System.Windows.Forms.Button btAddStudent;
        private System.Windows.Forms.Button btRemoveStudent;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lbStaffNumber;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lbUniversity;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lbStudentNumber;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lbAccommodationPayment;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lbAdress;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btCalculate;
        private System.Windows.Forms.DateTimePicker dtTo;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.CheckBox checkBox;
        private System.Windows.Forms.DateTimePicker dtSince;
        private System.Windows.Forms.DataGridView table;
    }
}

