﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Campus.Model
{
    public partial class AddForm : Form
    {
        public AddForm()
        {
            InitializeComponent();
            btSubmit.Click += BtSubmit_Click;
            btCancel.Click += BtCancel_Click;
        }

        public string result;

        private void BtCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void BtSubmit_Click(object sender, EventArgs e)
        {
            string reason = null;
            string[] arr = new string[] { tbSurname.Text, tbName.Text, tbGroup.Text };
            if (string.IsNullOrEmpty(tbName.Text))
            {
                reason = "Пустое поле ввода имени";
            } else if (string.IsNullOrEmpty(tbSurname.Text))
            {
                reason = "Пустое поле ввода фамилии";
            } else if (string.IsNullOrEmpty(tbGroup.Text))
            {
                reason = "Пустое поле ввода группы";
            } else
            {
                foreach (char i in tbName.Text)
                {
                    if (Char.IsDigit(i))
                    {
                        reason = "Имя не может содержать цифр";
                        break;
                    }
                }
                foreach (char i in tbSurname.Text)
                {
                    if (Char.IsDigit(i))
                    {
                        reason = "Фамилие не может содержать цифр";
                        break;
                    }
                }

                foreach (string j in arr)
                {
                    foreach (char i in j)
                    {
                         if (i == ' ')
                         {
                            reason = "Поля ввода не могут иметь пробелов";
                         }
                    }
                }
            }
            if (null == reason)
            {
                result = $"{arr[0]} {arr[1]} {arr[2]}";
                DialogResult = DialogResult.OK;
            } else
            {
                MessageBox.Show(this, reason, "Ошибка", 
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

        }
    }
}
