﻿using Campus.Model;
using System;
using System.Text;
using System.Windows.Forms;
using System.Collections.Generic;

namespace Campus
{
    public partial class MainForm : Form
    {
        private BindingSource _bs;
        private Model.Campus campus;

        public MainForm()
        {
            InitializeComponent();

            // Initializing campus object with data for form not to be empty
            string[] stud = new string[]
            {
                "Акименко Богдан КИУКИ-18-4",
                "Безруков Богдан ИРТЗИу-19-1"
            };
            campus = new Model.Campus(20, 5,
                "Харьковский национальный университет радиоэлектроники",
                stud, 520, "Харьков, Проспект Людвига Свободы 51Б");
#if (DEBUG) 
            var cloned = (Model.Campus) campus.Clone();
            cloned.Students.RemoveAt(0);
            Console.WriteLine("Cloned campus:");
            Console.WriteLine(cloned);
            Console.WriteLine("\n\nПервичный обьект:");
            Console.WriteLine(campus);
#endif

            _bs = new BindingSource();
            _bs.DataSource = campus.Students;
            table.DataSource = _bs;

            lbStaffNumber.Text = campus.StaffNumber.ToString();
            lbUniversity.Text = campus.UniversityName;
            lbStudentNumber.Text = campus.StudentsNumber.ToString();
            lbAccommodationPayment.Text = $"{campus.AccommodationPayment} грн";
            lbAdress.Text = campus.Adress;
            roomsNumber.ValueChanged += RoomsNumber_ValueChanged;
            roomsNumber.Value = campus.RoomsNumber;
            btAddStudent.Click += BtAddStudent_Click;
            btRemoveStudent.Click += BtRemoveStudent_Click;
            btCalculate.Click += BtCalculate_Click;
            checkBox.CheckedChanged += CheckBox_CheckedChanged;
            table.CellValidating += Table_CellValidating; ;
        }

        private void Table_CellValidating(object sender,
                    DataGridViewCellValidatingEventArgs e)
        {
            string s = e.FormattedValue.ToString();
            string reason = null;
            if (string.IsNullOrEmpty(s))
            {
                reason = "Поле ввода не может быть пустым!";
            } else
            {
                foreach (char i in s)
                {
                    if (i == ' ')
                    {
                        reason = "Поле ввода не может содержать пробелов!";
                        break;
                    }
                }
                if (null == reason && (0 == e.ColumnIndex || 1 == e.ColumnIndex))
                {
                    foreach (char i in s)
                    {
                        if (Char.IsDigit(i))
                        {
                            reason = "Это поле не может содержать цифр!";
                            break;
                        }
                    }
                }
            }
            if (null != reason)
            {
                MessageBox.Show(reason, "Ошибка!", MessageBoxButtons.OK,
                    MessageBoxIcon.Exclamation);
                e.Cancel = true;
            } else
            {
                Student st = campus.Students[e.RowIndex];
                switch (e.ColumnIndex)
                {
                    case 0:
                        st.Surname = s;
                        break;
                    case 1:
                        st.Name = s;
                        break;
                    case 3:
                        st.Group = s;
                        break;
                }
            }
        }

        private void CheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox.Checked)
            {
                campus.StaffNumber += 5;
            } else
            {
                campus.StaffNumber -= 5;
            }
            lbStaffNumber.Text = campus.StaffNumber.ToString();
        }

        private void BtCalculate_Click(object sender, EventArgs e)
        {
            DateTime s = dtSince.Value;
            DateTime to = dtTo.Value;
            string reason = null;
            if (s.Date == to.Date)
            {
                reason = "Промежуток не может начинаться и заканчиваться в тот же день.\n" +
                    "Установите корректный промежуток.";
            } else if (s.CompareTo(to) == 1)
            {
                reason = "Начало периода следует позже конца!";
            }
            if (null == reason)
            {
                var sb = new StringBuilder();
                sb.Append("Примерная прибыль за период должна составить: ");
                if (checkBox.Checked)
                {
                    sb.Append(campus.ProfitWithCanteen(s, to).ToString());
                } else
                {
                    sb.Append(campus.EvaluateProfit(s, to).ToString());
                }
                sb.Append(" грн.");
                MessageBox.Show(sb.ToString(), "Прибыль",
                MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show(reason, "Ошибка!", 
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void BtRemoveStudent_Click(object sender, EventArgs e)
        {
            var rows = table.SelectedRows;
            if (0 == rows.Count)
            {
                MessageBox.Show("Выберите строки таблицы, которые нужно удалить.", 
                    "Не выбрано", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else
            {
                DialogResult res = MessageBox.Show(this, "Вы уверены?", "Внимание!",
                    MessageBoxButtons.YesNo);
                if (res == DialogResult.Yes)
                {
                    for (int i = 0; i < rows.Count; ++i)
                    {
                        campus.EvictStudent(rows[i].Index);
                    }
                    _bs.ResetBindings(true);
                    lbStudentNumber.Text = campus.StudentsNumber.ToString();
                }
            }
        }

        private void BtAddStudent_Click(object sender, EventArgs e)
        {
            AddForm dialog = new AddForm();
            dialog.StartPosition = FormStartPosition.CenterParent;
            // Show testDialog as a modal dialog and determine if DialogResult = OK.
            if (dialog.ShowDialog(this) == DialogResult.OK)
            {
                campus.AddStudent(dialog.result);
                lbStudentNumber.Text = campus.StudentsNumber.ToString();
                _bs.ResetBindings(true);
            } 
        }

        
        private void RoomsNumber_ValueChanged(object sender, EventArgs e)
        {
            campus.RoomsNumber = Decimal.ToUInt32(roomsNumber.Value);
        }
    }

    public static class CampusExtension
    {
        public static decimal ProfitWithCanteen(this Model.Campus campus,
                DateTime since, DateTime to)
        {
            DateTime s = since.Date;
            DateTime t = to.Date;
            TimeSpan interval = t - s;
            return 1.2m * interval.Days * 
                (campus.AccommodationPayment * campus.StudentsNumber
                - 100m * (campus.StaffNumber - 5)) / 30m;
        }
    }
}


