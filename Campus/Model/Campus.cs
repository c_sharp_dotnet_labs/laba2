﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Campus.Model
{
    public class Student
    {
        public string Surname { get; set; }
        public string Name { get; set; }
        public string Group { get; set; }

        // information --- surname, name, group separated by a space
        public Student(string information)
        {
            int j = information.IndexOf(' ');
            Surname = information.Substring(0, j);
            ++j; // go to the next character after the space
            int k = information.IndexOf(' ', j);
            Name = information.Substring(j, k - j);
            Group = information.Substring(k + 1, information.Length - k - 1);
        }
        public override string ToString()
        {
            return $"{Surname} {Name} {Group}";
        }
    }

    public class Campus : ICloneable
    {
        public uint RoomsNumber { get; set; }
        public uint StaffNumber { get; set; }
        public string UniversityName { get; set; }
        public List<Student> Students { get; set; }
        public uint StudentsNumber { get => Convert.ToUInt32(Students.Count); }
        public decimal AccommodationPayment { get; set; }
        public string Adress { get; set; }

        public Campus(uint roomsNumber, uint staffNumber, string universityName, 
            string[] studs, decimal accommodationPayment, string adress)
        {
            Students = new List<Student>();
            RoomsNumber = roomsNumber;
            StaffNumber = staffNumber;
            UniversityName = universityName;
            foreach (string i in studs)
            {
                Students.Add(new Student(i));
            }
            if (accommodationPayment < 0) {
                throw new ArgumentException("Negative accommodation payment!");
            } else
            {
                AccommodationPayment = accommodationPayment;
            }
            Adress = adress;

        }

        public void IncreaseRoomsNumber() { ++RoomsNumber; }

        public void AddStudent(string s)
        {
            Students.Add(new Student(s));
        }

        // removes student from a collection
        public void EvictStudent(int index)
        {
            Students.RemoveAt(index);
        }

        public decimal EvaluateProfit(DateTime since, DateTime to)
        {
            DateTime s = since.Date;
            DateTime t = to.Date;
            TimeSpan interval = t - s;
            return interval.Days * (AccommodationPayment * StudentsNumber 
                - 100m * StaffNumber) / 30m;
        }

        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("\n-----------------------");
            sb.Append($"\nКоличество комнат: {RoomsNumber}");
            sb.Append($"\nКоличество персонала: {StaffNumber}");
            sb.Append($"\nУниверситет, которому кампус принадлежит: {UniversityName}");
            sb.Append($"\nКоличество студентов: {StudentsNumber}");
            sb.Append("\nСписок студентов:");
            foreach (Student s in Students)
            {
                sb.Append($"\n{s}");
            }
            sb.Append($"\nМесячная плата за проживание: {AccommodationPayment}");
            sb.Append($"\nАдрес студенческого кампуса: {Adress}");
            return sb.ToString();
        }

        public object Clone()
        {
            Campus cloned = (Campus) MemberwiseClone();
            cloned.Students = new List<Student>(Students);
            return cloned;
        }
    }
}
